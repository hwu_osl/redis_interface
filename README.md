Redis_Interface Package
=======================

This package contains the src for the redis key-value store database.
Extensive documentation can be found at: http://redis.io/

There is also an Interface python wrapper class which uses the redis python module
to access the server, but uses the pickle python module to convert the value field
to strings whether you give it a string, integer, or any python object - according
to: https://docs.python.org/2/library/pickle.html


## Install Redis on Ubuntu using PPA ##

The latest Redis version can be installed using the official Ubuntu PPA. This is the recommended way of having your Redis instance installed system-wide. In this way the server will be started automatically by the init system during the boot. Configuration files can be found inside `/etc/redis` folder.

```
# Install redis
sudo add-apt-repository ppa:chris-lea/redis-server
sudo apt-get install redis-server

# Start redis
sudo /etc/init.d/redis-server start 
```

   **OR**

## Install Redis from source ##
Download and make:
```
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
```
---------
---------
## IMPORTANT ##

Install python interface for redis.

```
sudo pip install redis
(https://github.com/andymccurdy/redis-py)
```

Commands for starting the server:
```
cd redis-stable/src/
./redis-server
```

Simple Client for quickly checking items in database:
```
cd redis-stable/src/
./redis-client
SET key value
GET key
```
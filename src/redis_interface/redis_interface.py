#! /usr/bin/env python
# Created 20/05/2014
# Author: Gordon Frost
# Version: V1.0
import redis
import pickle

class Interface(object):
    def __init__(self):
        ''' create the redis database server client object '''
        self.redis_server = redis.StrictRedis(host='localhost', port=6379, db=0)

    def set_data(self, key, value):
        ''' key can be str or int, pickle used to convert python objs to str values '''
        self.redis_server.set(key, pickle.dumps(value))

    def get_data(self, key):
        ''' key can be str or int, pickle used to convert str back to orig obj type '''
        result_str = self.redis_server.get(key)
        if result_str == None:
            print("No key of %s in database" % key)
            return
        return pickle.loads(result_str)
